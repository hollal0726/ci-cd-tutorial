from flask import request, Flask
import json

app = Flask(__name__)

@app.route('/')
def enterance():
    return "hello from gitlab"

@app.route('/plus_one')
def plus_one():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x + 1})

if __name__ == "__main__":
    app.run()

